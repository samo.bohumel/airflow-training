import datetime

from airflow import DAG
from airflow.providers.postgres.operators.postgres import PostgresOperator

#need to add connection profile: In the menu Admin -> Connections -> + 
#fill it with db connection details and save, then you can use is nder name Conn_id

with DAG(
    dag_id="ws_mkt_dpm_create_table",
    start_date=datetime.datetime(2022, 3, 25),
    schedule_interval="@once",
    catchup=False, tags=["samuel"]
) as dag:

    create_table = PostgresOperator(
        task_id="crate",
        postgres_conn_id="greenplum",
        sql="sql/ws_mkt_dpm_create.sql", #query from file or direct input
        #   sql="""
        #   SELECT columns
        #   FROM table
        #   LIMIT 1000 ;
        #   """
    )

    insert_into = PostgresOperator(
        task_id="insert",
        postgres_conn_id="greenplum",
        sql="sql/ws_mkt_dpm_insert.sql",
    )

    create_table >> insert_into

    #Use some query, if you use this in example, do not forget to drop that table 