
import airflow
import json
from airflow import DAG
import datetime
from airflow.operators.http_operator import SimpleHttpOperator
from airflow.operators.python_operator import PythonOperator
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from airflow.models import Variable

#this DAG sends a HTTP request to site https://gorest.co.in/public/v2/users and prints the response
#you need to add https://gorest.co.in/public/v2/ to your Connections

class BluePythonOperator(PythonOperator):       #extended standard class, just different color of a node
    ui_color = "#2fb5d6"
    ui_fgcolor = "white"

def send_mail(ti) -> None:

    sender_email = "" #if you want to try this one I will give you source email address
    receiver_email = "" #insert yours 
    

    message = MIMEMultipart("alternative")
    message["Subject"] = "Email to myself"
    message["From"] = sender_email
    message["To"] = receiver_email
    response = ti.xcom_pull(task_ids=['get_request'])

    # Create the plain-text and HTML version of your message
    html = """\
    <html>
        <body>
        <h1>Hello, this is an email send from Samuel's code</h1>
        <p>Hi,<br>
            If you are reading this, then task was successfuly executed.
            You can rest now.<br>
            Here are data from get request: <br>
    """
    response = response[0]
    for person in response:
        html = html + json.dumps(person)
        html = html + "<br>"
    
    html = html + """
        </p>
        </body>
        <footer>
            Samuel Bohumel
        </footer>
    </html>
    """

    # Turn these into plain/html MIMEText objects
    part2 = MIMEText(html, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part2)

    # Create secure connection with server and send email
    with smtplib.SMTP("smtp-outbound.ins.dell.com") as server:
        server.sendmail(
            sender_email, receiver_email, message.as_string()
        )


def print_previous(ti) -> None:
    response = ti.xcom_pull(task_ids=['get_request'])
    response = response[0]
    for person in response:
        print(person)

with DAG(
    dag_id="http_get",
    start_date=datetime.datetime(2022, 3, 25),
    schedule_interval="@once",
    catchup=False,
    tags=["samuel", "lesson"]
) as dag:

    task = SimpleHttpOperator(
    task_id='get_request',
    method='GET',
    endpoint='users/',
    http_conn_id="api",
    response_filter=lambda response: json.loads(response.text),
    log_response=True,
    dag=dag)

    task_save = BluePythonOperator(
        task_id='print_previous',
        python_callable=print_previous,
        dag=dag
    )

    send_email = PythonOperator( 
        task_id='send_email', 
        python_callable=send_mail, 
        dag=dag)
    

    task >> task_save >> send_email