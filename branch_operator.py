from airflow import DAG
from random import randint
from airflow.operators.python import PythonOperator, BranchPythonOperator
from airflow.operators.bash import BashOperator
from airflow.utils.edgemodifier import Label


import random
from datetime import datetime

class GreenPythonOperator(PythonOperator):      #create new class and change color of nodes
    ui_color = "#19e664"
    ui_fgcolor = "white"

class GreenBranchPythonOperator(BranchPythonOperator):
    ui_color = "#19e664"
    ui_fgcolor = "white"

class GreenBashOperator(BashOperator):
    ui_color = "#19e664"
    ui_fgcolor = "white"

def _choose_best_model(ti):
    accuracies = ti.xcom_pull(task_ids=[
        'node1',
        'node2',
        'node3'
    ])
    best_accuracy = max(accuracies)
    if (best_accuracy > 7):
        return 'accurate'
    return 'inaccurate'




def _training_model():
    return random.randint(1, 10)

with DAG("my_dag_with_branches", start_date=datetime(2021, 1, 1),
    schedule_interval="@daily", catchup=False,tags=["samuel"]) as dag:

        node1 = GreenPythonOperator(
            task_id="node1",
            python_callable=_training_model
        )

        node2 = PythonOperator(
            task_id="node2",
            python_callable=_training_model
        )

        node3 = PythonOperator(
            task_id="node3",
            python_callable=_training_model
        )

        choose_best_model = GreenBranchPythonOperator(
            task_id="choose_best_model",
            python_callable=_choose_best_model
        )

        accurate = GreenBashOperator(
            task_id="accurate",
            bash_command="echo 'accurate'"
        )

        inaccurate = BashOperator(
            task_id="inaccurate",
            bash_command="echo 'inaccurate'"
        )

        #dependencies
        [node1,node2,node3] >> Label("Random number 1-10") >> choose_best_model 
        choose_best_model >> Label("when max value > 8 ") >> accurate
        choose_best_model >> Label("else") >> inaccurate









