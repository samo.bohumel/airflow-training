from airflow import DAG

from airflow.operators.python import PythonOperator, BranchPythonOperator
from airflow.operators.bash import BashOperator

import random
from datetime import datetime


def my_function():  #python function
    string = "Keep calm and code."
    print(string)
    return string           #you can see return value in XCom of a task (cllick Graph -> node pyhon_task -> Log -> XCom)



with DAG("python_function", start_date=datetime(2021, 1, 1),
    schedule_interval="@daily", catchup=False,tags=["samuel"]) as dag:

    bash_task = BashOperator(task_id='bash_task', bash_command="echo 'command executed from BashOperator'")
        
    node1 = PythonOperator(
        task_id="python_task",
        python_callable=my_function
    )

        

    bash_task >> node1









