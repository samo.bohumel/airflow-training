from airflow import DAG
from airflow.utils.dates import days_ago
from datetime import datetime
from airflow.operators.python import PythonOperator

#example how to use XCOM to get info between nodes in DAG

def push(**kwargs):
    value_1 = [1, 2, 3]
    #push by method 
    kwargs['ti'].xcom_push(key='value from pusher 1', value=value_1)

def push_by_returning(**kwargs):
    value_2 = {'a': 'b'}
    # Pushes an XCom just by returning it
    return value_2      #key is "return_value"

def puller(**kwargs):
    #Pull all previously pushed XComs and check if the pushed values match the pulled values.
    ti = kwargs['ti']
    # get value_1
    pulled_value_1 = ti.xcom_pull(key='value from pusher 1', task_ids='push')
    print("Value 1: ", pulled_value_1)
    # get value_2
    pulled_value_2 = ti.xcom_pull(task_ids='push_by_returning')
    print("Value 2: ", pulled_value_2)

with DAG(
    dag_id="XCom_push_pull",
    start_date=datetime(2022, 3, 25),
    schedule_interval="@once",
    catchup=False,
    tags=["samuel", "lesson"]
) as dag:

    push1 = PythonOperator(
    task_id='push',provide_context=True, #provide context is for getting the TI (task instance ) parameters
    dag=dag,
    python_callable=push,
    )

    push2 = PythonOperator(
        task_id='push_by_returning',
        dag=dag,
        python_callable=push_by_returning,
    )

    pull = PythonOperator(
        task_id='puller',provide_context=True,#provide context is for getting the TI (task instance ) parameters
        dag=dag,
        python_callable=puller,
    )
    # set of operators, push1,push2 are upstream to pull
    [push1, push2] >> pull