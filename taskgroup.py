from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator
from airflow.utils.task_group import TaskGroup
from datetime import datetime, timedelta

#Org tip: this is how to use taskgroup to keep nodes organised

with DAG(dag_id='taskgroup', schedule_interval=None, start_date=datetime(2020, 1, 1), catchup=False, tags=["samuel","piatok"]) as dag:
    
    
    t0 = DummyOperator(task_id='start')

    # Start Task Group definition
    with TaskGroup(group_id='group1') as tg1:
        t1 = DummyOperator(task_id='task1')
        t2 = DummyOperator(task_id='task2')

        t1 >> t2
    # End Task Group definition
        
    t3 = DummyOperator(task_id='end')

    # Set Task Group' dependencies
    t0 >> tg1 >> t3         #taskgroup look like single node here
    #you can nest these taskgroups