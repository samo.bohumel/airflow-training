import airflow 
from datetime import timedelta 
from airflow import DAG 
from datetime import datetime, timedelta 
from airflow.operators.python_operator import PythonOperator 
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def send_mail():
    sender_email = "" #if you want to try this one I will give you source email address
    receiver_email = "" #insert yours 
    

    message = MIMEMultipart("alternative")
    message["Subject"] = "Email to myself"
    message["From"] = sender_email
    message["To"] = receiver_email

    # Create the plain-text and HTML version of your message

    html = """\
    <html>
        <body>
        <h1>Hello, this is an email sent from Samuel's code</h1>
        <p>Hi,<br>
            If you are reading this, then task was successfuly executed.
            You can rest now.
        </p>
        </body>
        <footer>
            Samuel Bohumel
        </footer>
    </html>
    """

    # Turn these into plain/html MIMEText objects
    part2 = MIMEText(html, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part2)

    # Create secure connection with server and send email
    with smtplib.SMTP("smtp-outbound.ins.dell.com") as server:
        server.sendmail(
            sender_email, receiver_email, message.as_string()
        )

def start_task(): 
    print("task started") 


with  DAG( 
    dag_id = 'email_demo', 
    start_date=datetime(2021, 1, 1),
    schedule_interval="@monthly",
    tags=["samuel"],
    description='send email after finishing tasks', 
    ) as dag_email:

    start_task = PythonOperator( 
        task_id='executetask', 
        python_callable=start_task,
        dag=dag_email) 

    send_email = PythonOperator( 
        task_id='send_email', 
        python_callable=send_mail, 
        dag=dag_email)

    start_task >> send_email